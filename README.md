# Como colocar sua foto como participante

1. Faça um Fork do repositório.
2. Clone o repositório do GitLab para sua máquina local: `git clone https://gitlab.com/allythy/minicurso-git-gitlab.git`
3. Crie uma branch para adicionar sua foto: `git checkout -b novoParticipante`
4. Crie um arquivo html com o seu nome e sobrenome na pasta de `users`. Exemplo: josemaria.html
5. Pegue como exemplo a estrutura do arquivo allythyrenan.html (na pasta **users**) e altere com suas informações que são: nome, GitLab e foto
6. Adicioner o arquivo novo: `git add .`
7. Commit suas alterações: `git commit -m "Novo participante Allythy Rennan"`
7. Mande o Push para sua branch: `git push origin novoParticipante`
8. Entre no GitLab e envie o seu pull request

## Rodando o projeto localmente

Primeiro, temos que criar o nosso ambiente virtual:

```
virtualenv env
```

Ativando o ambiente virtual:

```
source env/bin/activate
```

Instalando as dependências do projeto:

```
pip install -r requirements.txt
```

Rodando a aplicação:

```
python app.py
```

OBS: Se você não souber instalar o pip no Debian e derivados eu fiz um post ensinando como fazer, [você pode ler aqui.](https://allythy.github.io/Como-instalar-o-pip-para-gerenciar-pacotes-do-Python-no-GNU-Linux)
